module.exports = {
    extends: [
        "@commitlint/config-conventional"
    ],
    rules: {
        "type-enum": [2, "always", [
            "build",
            "change",
            "chore",
            "ci",
            "deprecate",
            "docs",
            "feat",
            "fix",
            "perf",
            "refactor",
            "remove",
            "revert",
            "security",
            "style",
            "test"
        ]]
    }
}