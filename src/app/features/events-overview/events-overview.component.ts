import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

import {
  Observable,
  BehaviorSubject,
  combineLatest,
  map,
  fromEvent,
  takeUntil,
  debounceTime,
} from 'rxjs';

import {
  AccordionContentDirective,
  AccordionComponent,
  AccordionHeaderDirective,
  AccordionItemDirective,
  TabComponent,
  TabsComponent,
} from '@ui-kit';

import { DestroyService } from '@shared-services';

import { EventsListComponent, CalendarComponent, EventsMenuComponent } from './components';
import { EventsOverviewService } from './services';
import { EventsOverview, LeaguesByCountry } from './types';

@Component({
  selector: 'app-events-overview',
  standalone: true,
  imports: [
    AccordionContentDirective,
    AccordionComponent,
    AccordionHeaderDirective,
    AccordionItemDirective,
    CalendarComponent,
    CommonModule,
    EventsListComponent,
    EventsMenuComponent,
    TabComponent,
    TabsComponent,
  ],
  providers: [DestroyService, EventsOverviewService],
  templateUrl: './events-overview.component.html',
  styleUrls: ['./events-overview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventsOverviewComponent implements OnInit {
  private events$: Observable<EventsOverview[]> = this.eventsOverviewService.events$;

  leaguesByCountry$: Observable<LeaguesByCountry[]> = this.eventsOverviewService.leaguesByCountry$;

  showScrollTop: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  showScrollTop$: Observable<boolean> = this.showScrollTop.asObservable();

  selectedTab: BehaviorSubject<string> = new BehaviorSubject<string>('');

  filteredEvents$: Observable<EventsOverview[]> = combineLatest([
    this.selectedTab,
    this.events$,
  ]).pipe(map(([selectedTab, events]) => this.filterEvents(selectedTab, events)));

  constructor(
    private eventsOverviewService: EventsOverviewService,
    private route: ActivatedRoute,
    private destroyed$: DestroyService,
  ) {}

  ngOnInit(): void {
    this.eventsOverviewService.getEventsData();
    // this.route.paramMap
    //   .pipe(takeUntil(this.destroyed$))
    //   .subscribe((params) =>
    //     this.eventsOverviewService.getEventsData(params.get('event-overview') as string),
    //   );

    fromEvent(window, 'scroll')
      .pipe(
        takeUntil(this.destroyed$),
        debounceTime(200),
        map(() => window.scrollY),
      )
      .subscribe((value) => {
        this.showScrollTop.next(value > 250);
      });
  }

  private filterEvents(criteria: string, events: EventsOverview[]): EventsOverview[] {
    if (criteria !== 'all') {
      return events.map((eventData) => {
        return {
          ...eventData,
          events: eventData.events.filter((match) => {
            return criteria === 'completed'
              ? match.endDate
              : this.checkIfActive(match.startDate, match.endDate);
          }),
        };
      });
    }
    return events;
  }

  private checkIfActive(startDate: string, endDate: string | null): boolean {
    return new Date().getTime() > new Date(startDate).getTime() && !endDate;
  }

  getEvents(date: string) {
    return date;
    // get events from service by date
  }

  scrollTop() {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }
}
