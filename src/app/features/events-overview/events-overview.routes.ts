import { Routes } from '@angular/router';
import { EventsOverviewComponent } from './events-overview.component';

export const EVENTS_OVERVIEW_ROUTES: Routes = [
  {
    path: ':event-name',
    component: EventsOverviewComponent,
  },
];
