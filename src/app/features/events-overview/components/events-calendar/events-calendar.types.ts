export interface DateType {
  title: string | null;
  value: string;
  selected: boolean;
}
