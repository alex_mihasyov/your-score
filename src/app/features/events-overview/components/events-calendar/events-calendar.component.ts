import { ChangeDetectionStrategy, Component, EventEmitter, Output, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BehaviorSubject, takeUntil } from 'rxjs';
import { DestroyService } from '@shared-services';
import { ClickOutsideDirective } from '@shared-directives';
import { DateType } from './events-calendar.types';

@Component({
  selector: 'app-events-calendar',
  standalone: true,
  imports: [CommonModule, ClickOutsideDirective],
  providers: [DestroyService],
  templateUrl: './events-calendar.component.html',
  styleUrls: ['./events-calendar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalendarComponent implements OnInit {
  showDates = false;

  dates$: BehaviorSubject<DateType[]> = new BehaviorSubject<DateType[]>([]);

  selectedDate$: BehaviorSubject<string> = new BehaviorSubject<string>(new Date().toISOString());

  minDateSelected$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  maxDateSelected$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  navigation$: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  @Output() dateSelection = new EventEmitter<string>();

  constructor(private destroyed$: DestroyService) {}

  ngOnInit() {
    // FILL ARRAY WITH 1 WEEK BEFORE AND 1 WEEK AFTER CURRENT DATE
    this.dates$.next(this.generateDates());

    this.selectedDate$.pipe(takeUntil(this.destroyed$)).subscribe((date) => {
      const currentDates = this.dates$.getValue();
      const currentDateIndex = currentDates.findIndex(
        (dateVal) => new Date(dateVal.value).getDate() === new Date(date).getDate(),
      );

      this.minDateSelected$.next(currentDateIndex === 0);
      this.maxDateSelected$.next(currentDateIndex === currentDates.length - 1);

      this.dates$.next(this.markDateAsSelected(currentDates, date));
      this.dateSelection.emit(date);
    });

    this.navigation$.pipe(takeUntil(this.destroyed$)).subscribe((dateStep) => {
      const currentDate = new Date().setDate(new Date(this.selectedDate$.getValue()).getDate());
      const currentDates = this.dates$.getValue();
      const resultDate = this.addDays(currentDate, dateStep);

      this.selectedDate$.next(resultDate);
      this.dates$.next(this.markDateAsSelected(currentDates, resultDate));
    });
  }

  private addDays(date: number, days: number): string {
    return new Date(new Date(date).setDate(new Date(date).getDate() + days)).toISOString();
  }

  private checkIfToday(date: string): boolean {
    return new Date().getDate() === new Date(date).getDate();
  }

  private generateDates(): DateType[] {
    const startDate = new Date().setDate(new Date().getDate() - 7);
    return [...new Array(15)].map((_, index) => {
      const date = this.addDays(startDate, index);
      return {
        title: this.checkIfToday(date) ? null : date,
        value: date,
        selected: new Date().getDate() === new Date(date).getDate(),
      };
    });
  }

  private markDateAsSelected(dates: DateType[], dateValue: string): DateType[] {
    return dates.map((date) => {
      return {
        ...date,
        selected: new Date(date.value).getDate() === new Date(dateValue).getDate(),
      };
    });
  }

  selectDate(date: string) {
    this.selectedDate$.next(date);
    this.showDates = false;
  }

  onNext() {
    this.navigation$.next(1);
  }

  onPrevious() {
    this.navigation$.next(-1);
  }
}
