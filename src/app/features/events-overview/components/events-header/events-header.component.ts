import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-events-header',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './events-header.component.html',
  styleUrls: ['./events-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventsHeaderComponent {
  @Input() isInFavourites = false;

  @Input() isHighlighted = false;

  @Input() logo = '';

  @Input() country = '';

  @Input() league = '';
}
