import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AccordionContentDirective,
  AccordionComponent,
  AccordionHeaderDirective,
  AccordionItemDirective,
} from '@ui-kit';
import { EventsHeaderComponent } from '../events-header/events-header.component';
import { EventsItemComponent } from '../events-item/events-item.component';
import { EventsOverview } from '../../types';

@Component({
  selector: 'app-events-list',
  standalone: true,
  imports: [
    CommonModule,
    AccordionContentDirective,
    AccordionComponent,
    AccordionHeaderDirective,
    AccordionItemDirective,
    EventsHeaderComponent,
    EventsItemComponent,
  ],
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventsListComponent {
  @Input() events: EventsOverview[] = [];

  checkHighRate(rate: number): boolean {
    return rate >= 5;
  }

  checkInFavourites(rate: number): boolean {
    // CHECK IN LOCAL STORAGE
    return rate >= 5;
  }
}
