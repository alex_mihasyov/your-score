import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DividerComponent } from '@ui-kit';
import { Event } from '../../types';

@Component({
  selector: 'app-events-item',
  standalone: true,
  imports: [CommonModule, DividerComponent],
  templateUrl: './events-item.component.html',
  styleUrls: ['./events-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventsItemComponent implements OnInit {
  @Input() event!: Event;

  isActive!: boolean;

  isUpcoming!: boolean;

  isSubscribed = false;

  ngOnInit(): void {
    this.isUpcoming = new Date().getTime() < new Date(this.event.startDate).getTime();

    this.isActive =
      Boolean(!this.event.endDate) &&
      new Date().getTime() > new Date(this.event.startDate).getTime();
  }

  toggleSubscribe() {
    // LOCAL STORAGE
    this.isSubscribed = !this.isSubscribed;
  }

  getMinutesFromStart(start: string): number {
    const diff = new Date().getTime() - new Date(start).getTime();
    return Math.ceil(diff / (1000 * 60));
  }
}
