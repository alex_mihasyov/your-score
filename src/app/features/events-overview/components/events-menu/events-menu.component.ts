import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DividerComponent } from '@ui-kit';
import { LeaguesByCountry } from '../../types';

@Component({
  selector: 'app-events-menu',
  standalone: true,
  imports: [CommonModule, DividerComponent],
  templateUrl: './events-menu.component.html',
  styleUrls: ['./events-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventsMenuComponent {
  expandedList: Set<number> = new Set<number>();

  more = false;

  @Input() leaguesByCountry!: LeaguesByCountry[] | null;

  toggleCollapsed(index: number) {
    if (this.expandedList.has(index)) {
      this.expandedList.delete(index);
    } else {
      this.expandedList.add(index);
    }
  }
}
