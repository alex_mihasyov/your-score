import { Injectable } from '@angular/core';
import { Observable, delay, shareReplay, Subject, ReplaySubject } from 'rxjs';
// import { WebsocketService } from '@shared-services';
import { LeaguesByCountry, EventsOverview } from '../../types';
import { countryList, eventsOverview } from '../../mocks';

@Injectable({
  providedIn: 'root',
})
export class EventsOverviewService {
  // Leagues by country for menu
  leaguesByCountry: Subject<LeaguesByCountry[]> = new ReplaySubject<LeaguesByCountry[]>(1);

  leaguesByCountry$: Observable<LeaguesByCountry[]> = this.leaguesByCountry
    .asObservable()
    .pipe(delay(1000));

  // Events for table
  events: Subject<EventsOverview[]> = new ReplaySubject<EventsOverview[]>(1);

  events$: Observable<EventsOverview[]> = this.events
    .asObservable()
    .pipe(delay(1000), shareReplay(1));

  // constructor(private wsService: WebsocketService) {}

  getEventsData() {
    // this.wsService.on(event).subscribe((data) => this.events.next(data))
    this.leaguesByCountry.next(countryList);
    this.events.next(eventsOverview);
  }
}
