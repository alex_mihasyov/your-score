import { Team, League, Country } from '@core-models';

export interface Event {
  id: string;
  startDate: string;
  endDate: string | null;
  homeTeam: Team;
  awayTeam: Team;
  homeTeamScore: number | null;
  awayTeamScore: number | null;
}

export interface EventsOverview {
  league: League;
  country: Country;
  events: Event[];
}

export interface LeaguesByCountry extends Country {
  leagues: League[] | null;
}
