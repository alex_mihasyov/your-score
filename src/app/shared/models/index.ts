export interface Team {
  id: string;
  name: string;
  nameRu: string;
  emblem: string;
}

export interface League {
  id: string;
  name: string;
  rate: number;
}

export interface Country {
  id: string;
  name: string;
  nameRu: string;
  flag: string;
}
