import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { WebSocketSubject, webSocket } from 'rxjs/webSocket';

export interface MessageEvent<T> {
  event: string;
  data: T;
}

export interface WebSocketConfig {
  url: string;
  reconnectInterval?: number;
  reconnectAttempts?: number;
}

type ReconnectOptions = Required<Pick<WebSocketConfig, 'reconnectAttempts' | 'reconnectInterval'>>;

@Injectable({
  providedIn: 'root',
})
export class WebsocketService {
  websocket$!: WebSocketSubject<unknown> | null;

  messagesSubject: Subject<MessageEvent<unknown>> = new Subject<MessageEvent<unknown>>();

  messages$: Observable<MessageEvent<unknown>> = this.messagesSubject.asObservable();

  connect(wsCofig: WebSocketConfig) {
    const { reconnectAttempts = 3, reconnectInterval = 3000 } = wsCofig;
    if (!this.websocket$ || this.websocket$.closed) {
      this.websocket$ = this.createSocket(wsCofig);
      this.websocket$.subscribe({
        next: (event) => this.messagesSubject.next(event as MessageEvent<unknown>),
        error: () => this.reconnect({ reconnectAttempts, reconnectInterval }),
      });
    }
  }

  reconnect(options: ReconnectOptions): ReconnectOptions {
    return options;
  }

  createSocket(wsCofig: WebSocketConfig): WebSocketSubject<unknown> {
    return webSocket({
      ...wsCofig,
      closeObserver: { next: () => (this.websocket$ = null) },
    });
  }

  sendMessage(message: MessageEvent<unknown>) {
    this.websocket$?.next(message);
  }
}
