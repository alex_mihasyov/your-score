import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: '[appAccordionContent]',
  standalone: true,
})
export class AccordionContentDirective {
  templateRef: TemplateRef<unknown>;

  constructor(templateRef: TemplateRef<unknown>) {
    this.templateRef = templateRef;
  }
}
