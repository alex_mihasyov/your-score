import { Directive, Input, ContentChild } from '@angular/core';
import { AccordionContentDirective } from './accordion-content.directive';
import { AccordionHeaderDirective } from './accordion-header.directive';

@Directive({
  selector: '[appAccordionItem]',
  standalone: true,
})
export class AccordionItemDirective {
  @Input() title = '';

  @Input() disabled = false;

  @Input() expanded = false;

  @ContentChild(AccordionContentDirective) content!: AccordionContentDirective;

  @ContentChild(AccordionHeaderDirective) header!: AccordionHeaderDirective;
}
