import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: '[appAccordionHeader]',
  standalone: true,
})
export class AccordionHeaderDirective {
  templateRef: TemplateRef<unknown>;

  constructor(templateRef: TemplateRef<unknown>) {
    this.templateRef = templateRef;
  }
}
