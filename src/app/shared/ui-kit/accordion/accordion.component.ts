import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ContentChildren,
  QueryList,
  AfterContentInit,
  OnDestroy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { map, startWith, takeUntil, Subject } from 'rxjs';
import { AccordionItemDirective } from './directives/accordion-item.directive';

@Component({
  selector: 'app-accordion',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccordionComponent implements AfterContentInit, OnDestroy {
  expandedList = new Set<number>();

  private destroyed$ = new Subject<void>();

  @Input() collapsing = true;

  @Input() withGap = false;

  @ContentChildren(AccordionItemDirective) items!: QueryList<AccordionItemDirective>;

  constructor(private cdr: ChangeDetectorRef) {}

  ngAfterContentInit(): void {
    this.items.changes
      .pipe(
        startWith(undefined),
        takeUntil(this.destroyed$),
        map(() => this.items.toArray()),
      )
      .subscribe((items) => {
        items.forEach((item, i) => {
          if (item.expanded) {
            this.expandedList.add(i);
          }
        });
        this.cdr.detectChanges();
      });
  }

  ngOnDestroy() {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  getToggleFunc(index: number) {
    return this.toggleCollapsed.bind(this, index);
  }

  toggleCollapsed(index: number) {
    return () => {
      if (this.expandedList.has(index)) {
        this.expandedList.delete(index);
      } else {
        if (this.collapsing) {
          this.expandedList.clear();
        }
        this.expandedList.add(index);
      }
    };
  }
}
