export interface ButtonProps {
  type: 'primary' | 'default' | 'text' | 'link';
}
