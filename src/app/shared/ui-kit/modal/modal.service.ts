import { Injectable, ComponentRef, ViewContainerRef, TemplateRef } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ModalComponent } from './modal-component/modal.component';

type CloseCallback = () => void;

@Injectable({ providedIn: 'root' })
export class ModalService {
  private modalRef!: ComponentRef<ModalComponent>;

  private modalSubscriber!: Subject<void>;

  constructor(private vcr: ViewContainerRef) {}

  open(
    modalBody: TemplateRef<unknown>,
    modalTitle?: string,
    onConfirm?: (close: CloseCallback) => Observable<void>,
  ): Observable<void> {
    this.modalRef = this.vcr.createComponent(ModalComponent);
    this.modalRef.setInput('title', modalTitle);
    this.modalRef.setInput('bodyContent', modalBody);
    this.modalRef.setInput('onConfirm', onConfirm);
    this.modalRef.instance.closeEvent.subscribe(() => this.close());
    this.modalSubscriber = new Subject();
    return this.modalSubscriber.asObservable();
  }

  close() {
    this.modalSubscriber.next();
    this.modalSubscriber.complete();
    this.modalRef.destroy();
  }
}
