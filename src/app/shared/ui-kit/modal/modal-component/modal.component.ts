import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Observable } from 'rxjs';
import { DividerComponent } from '../../divider';

@Component({
  selector: 'app-modal',
  standalone: true,
  imports: [CommonModule, DividerComponent],
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModalComponent implements AfterViewInit {
  @Input() bodyContent!: TemplateRef<unknown>;

  @Input() onConfirm!: () => Observable<unknown>;

  @Input() title = '';

  @Output() closeEvent = new EventEmitter();

  @Output() confirmEvent = new EventEmitter();

  @ViewChild('modal', {
    read: ViewContainerRef,
  })
  modalRef!: ViewContainerRef;

  ngAfterViewInit(): void {
    this.modalRef.createEmbeddedView(this.bodyContent);
  }

  close() {
    this.closeEvent.emit();
  }

  confirm() {
    this.confirmEvent.emit();
  }
}
