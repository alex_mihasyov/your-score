import { Directive, ElementRef, Output, EventEmitter, HostListener } from '@angular/core';

@Directive({
  selector: '[appClickOutside]',
  standalone: true,
})
export class ClickOutsideDirective {
  @Output() appClickOutside: EventEmitter<void> = new EventEmitter<void>();

  @HostListener('document:click', ['$event.target'])
  onClick(target: HTMLElement) {
    const clickedOutside = !this.elementRef.nativeElement.contains(target);
    if (clickedOutside) {
      this.appClickOutside.emit();
    }
  }

  constructor(private elementRef: ElementRef) {}
}
