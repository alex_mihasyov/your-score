import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { CommonModule, DOCUMENT } from '@angular/common';
import { ToggleComponent } from '@ui-kit';
import { ClickOutsideDirective } from '@shared-directives';

@Component({
  selector: 'app-burger-menu',
  standalone: true,
  imports: [CommonModule, ToggleComponent, ClickOutsideDirective],
  templateUrl: './burger-menu.component.html',
  styleUrls: ['./burger-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BurgerMenuComponent {
  isOpened = false;

  isDarkTheme = false;

  constructor(@Inject(DOCUMENT) private document: Document) {}

  openSettings() {}

  toggleOpened() {
    this.isOpened = !this.isOpened;
  }

  toggleTheme() {
    this.isDarkTheme = !this.isDarkTheme;
    this.document.body.classList.toggle('theme--dark');
  }
}
