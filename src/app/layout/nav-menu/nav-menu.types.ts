export interface Sport {
  id: string | number;
  rating: number;
  category: string;
  categoryRu: string;
  todayTotal: number;
}
