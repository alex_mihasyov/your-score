import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ClickOutsideDirective } from '@shared-directives';
import { Sport } from './nav-menu.types';

@Component({
  selector: 'app-nav-menu',
  standalone: true,
  imports: [CommonModule, RouterModule, ClickOutsideDirective],
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavMenuComponent {
  // Mock
  sports: Sport[] = [
    { id: 1, rating: 5, category: 'soccer', categoryRu: 'футбол', todayTotal: 22 },
    { id: 2, rating: 5, category: 'hockey', categoryRu: 'хоккей', todayTotal: 22 },
    { id: 3, rating: 5, category: 'tennis', categoryRu: 'теннис', todayTotal: 22 },
    { id: 4, rating: 5, category: 'basketball', categoryRu: 'баскетбол', todayTotal: 22 },
    { id: 5, rating: 5, category: 'volleyball', categoryRu: 'волейбол', todayTotal: 22 },
    { id: 15, rating: 5, category: 'handball', categoryRu: 'гандбол', todayTotal: 22 },
    { id: 6, rating: 5, category: 'baseball', categoryRu: 'бейсбол', todayTotal: 22 },
    { id: 7, rating: 5, category: 'golf', categoryRu: 'гольф', todayTotal: 22 },
    { id: 8, rating: 5, category: 'mma', categoryRu: 'мма', todayTotal: 22 },
    { id: 9, rating: 5, category: 'boxing', categoryRu: 'бокс', todayTotal: 22 },
    { id: 11, rating: 2, category: 'badminton', categoryRu: 'бадминтон', todayTotal: 22 },
    { id: 12, rating: 3, category: 'moto-sport', categoryRu: 'мото', todayTotal: 22 },
    { id: 13, rating: 3, category: 'water-polo', categoryRu: 'водное поло', todayTotal: 22 },
    { id: 14, rating: 3, category: 'darts', categoryRu: 'дартс', todayTotal: 22 },
  ];

  isOpened = false;

  selectedSport = 'soccer';

  toggleOpened() {
    this.isOpened = !this.isOpened;
  }

  selectSport(sportCategory: string) {
    this.selectedSport = sportCategory;
  }
}
