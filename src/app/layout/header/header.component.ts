import { ChangeDetectionStrategy, Component, ViewChild, TemplateRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ButtonComponent, ModalService } from '@ui-kit';
import { NavMenuComponent } from '../nav-menu';
import { BurgerMenuComponent } from '../burger-menu';
import { LoginComponent, SearchComponent } from '../modals';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    ButtonComponent,
    NavMenuComponent,
    BurgerMenuComponent,
    SearchComponent,
    LoginComponent,
  ],
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
  @ViewChild('search', { read: TemplateRef<SearchComponent> })
  searchModal!: TemplateRef<SearchComponent>;

  @ViewChild('login', { read: TemplateRef<LoginComponent> })
  loginModal!: TemplateRef<LoginComponent>;

  constructor(private modalSevice: ModalService) {}

  onSearchClick(): void {
    this.modalSevice.open(this.searchModal);
  }

  onLoginClick(): void {
    this.modalSevice.open(this.loginModal);
  }
}
