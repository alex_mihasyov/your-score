import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { ModalComponent, ModalService } from '@ui-kit';
import { HeaderComponent, FooterComponent } from './layout';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, HeaderComponent, FooterComponent, ModalComponent],
  providers: [ModalService],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {}
