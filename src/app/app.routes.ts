import { Routes } from '@angular/router';
import { EventsOverviewComponent } from './features/events-overview/events-overview.component';

export const routes: Routes = [
  {
    path: 'events/:event-overview',
    component: EventsOverviewComponent,
  },
  {
    path: '**',
    redirectTo: 'events/soccer',
  },
];
